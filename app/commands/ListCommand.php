<?php

namespace App\Commands;

use Vendor\Input\InputInterface;
use Vendor\Output\OutputInterface;

class ListCommand implements \Vendor\Command\CommandInterface
{
    public static $NAME = 'list';
    public static $INFO = 'показать список всех команд и опций';
    private $input;
    private $output;

    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $this->output->fwrite('<success>----------------------------------</success>');
        $this->output->fwrite('<success>Консольная утилита "это Судьба" :D</success>');
        $this->output->fwrite('<success>----------------------------------</success>');

        $this->output->fwrite(PHP_EOL . '<warning>Как использовать?</warning>');
        $this->output->fwrite('  command [arguments] [options]');

        $this->output->fwrite(PHP_EOL . '<warning>Опции:</warning>');
        $this->output->fwrite('<success>  -тутьss</success><info> - будет опция</info>');
        $this->output->fwrite('<success>  --итуть</success><info> - будет опция</info>');
        $this->output->fwrite('<success>  ---итда</success><info> - будет опция</info>');

        // здесь надо будет реализовать сканирование команд, ключей и описание параметров, чтобы потом
        // вместе с ключом -h выводит инфу о командах
        $this->output->fwrite(PHP_EOL . '<warning>Доступные команды:</warning>');
        $this->showListCommands();
    }

    private function showListCommands()
    {
        $commandsDir = './app/commands/';
        if (!is_dir($commandsDir)) {
            die("Отсутствует необходимая дирректория с командами '$commandsDir'");
        }
        // !!! вполне вероятно что есть метод который позволяет по пространству имени узнать все классы
        $commandList = scandir($commandsDir);
        array_shift($commandList); // delete .
        array_shift($commandList); // delete ..
        foreach ($commandList as $index => $command) {
            // !!! здесь бы надо использовать мутацию commandDir где слеши будут в другую стророну и названя с верним регистром первой буквы
            $command = "\App\Commands\\" . str_replace('.php', '', $command);
            $this->output->fwrite('<success>   '.$command::$NAME.'</success><info> - '.$command::$INFO.'</info>');
        }
    }
}