<?php

namespace App\Commands;

use Vendor\Input\InputInterface;
use Vendor\Output\OutputInterface;


class TestCommand implements \Vendor\Command\CommandInterface
{
    public static $NAME = 'test:test';
    public static $INFO = 'тестовая команда';
    private $input;
    private $output;

    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->input->addArgument('arg1')
            ->addArgument('arg2')
            ->addArgument('lehinArgument');
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $this->output->fwrite('<success>'.__METHOD__.'</success>');
        // Проверка на наличие опций
        if ($this->input->hasOption('-r')) {
            $this->output->fwrite('<success>Опция -r присутствует</success>');
        } else {
            $this->output->fwrite('<error>Опция -r НЕ присутствует</error>');
        }
        // получение ранее созданного аргумента
        $this->output->fwrite(
            "<warning>arg1:" . $this->input->getArgument('arg1') . '</warning>'
        );
        $this->output->fwrite(
            "<warning>arg2:" . $this->input->getArgument('arg2') . '</warning>'
        );
        $this->output->fwrite(
            "<warning>lehinArgument:" . $this->input->getArgument('lehinArgument') . '</warning>'
        );
        $this->output->fwrite(
            "<warning>your options:" .implode(" ",$this->input->getOptions()) . "</warning>"
        );
    }
}

