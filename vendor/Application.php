<?php

namespace Vendor;

use Vendor\Command;
use Vendor\Output\Output;
use Vendor\Input\ArgvInput;

/**
 * Основной класс, для выполнения всех команд aka Invoker aka Creator aka Точка входа
 * В целом он должен даже реализован как фабричный метод, или то что запускает команду, должен быть фабричным методом.
 */
class Application
{
    private $tokens;
    private $currentCommand;
    private $defaultCommand;
    private $commandList;
    private $output;
    private $input;

    public function __construct($tokens = null)
    {
        $this->output = new Output();
        // Обрабатываем поступающие данные из консоли (можно потом в отдельный класс переделать, чтобы больше возможностей добавить)
        if (null === $tokens) {
            die('Ошибка параметры переданы неверено.');
        }
        $this->commandList = [];

        // Парсим пользовательский ввод аргументов
        array_shift($tokens); // Убираем наименование запускаемого файла "console"
        $this->tokens = $tokens;
        $this->input = new ArgvInput($this->tokens);

        $this->defaultCommand = new \App\Commands\ListCommand($this->input, $this->output);
        $this->autoAddCommands(); // сгенерировал список строк с командами \App\Commands\TestCommand
        $this->findCurrentCommand();
    }

    /**
     * Основно метод
     * @return int
     */
    public function run()
    {
        $exitCode = 0;
        try {
            if (null === $this->currentCommand) {
                // запускаем команду по дефолту list
                $this->defaultCommand->execute();
            } else {
                $this->currentCommand->execute();
            }
        } catch (Exception $error) {
            echo $error->getMessage() . PHP_EOL;
            $exitCode = 1;
        }
        return $exitCode;
    }


    /**
     * Путь к дирректории к командам должна находиться в 'jarvis/app/commands'
     */
    public function autoAddCommands()
    {
        $commandsDir = './app/commands/';
        if (!is_dir($commandsDir)) {
            echo "Отсутствует необходимая дирректория с командами './app/commands'" . PHP_EOL;
        }
        // !!! вполне вероятно что есть метод который позволяет по пространству имени узнать все классы
        $this->commandList = scandir($commandsDir);
        array_shift($this->commandList); // delete .
        array_shift($this->commandList); // delete ..
        foreach ($this->commandList as $index => $command) {
            // !!! здесь бы надо использовать мутацию commandDir где слеши будут в другую стророну и названя с верним регистром первой буквы
            $this->commandList[$index] = "\App\Commands\\" . str_replace('.php', '', $command);
        }
    }


    private function findCurrentCommand()
    {
        $tempCommand = null;
        foreach ($this->commandList as $command) {
            if ($this->tokens[0] === $command::$NAME) {
                $tempCommand = $command;
                break;
            }
        }
        if (null === $tempCommand) {
            $this->output->fwrite('<warning>Предупреждение: Данной команды не существует</warning>');

        } else {
            $this->setCurrentCommand(new $tempCommand($this->input, $this->output));
        }
    }

    private function setCurrentCommand(Command\CommandInterface $command)
    {
        unset($this->commandList); // выгружаем
        $this->currentCommand = $command; // Создаем команду через интерфейс
    }

}