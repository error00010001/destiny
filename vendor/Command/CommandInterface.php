<?php

namespace Vendor\Command;

use Vendor\Input\InputInterface;
use Vendor\Output\OutputInterface;

/**
 * Единственный метод который должен быть исполнен в каждом ручном классе
 */
interface CommandInterface
{
    public function __construct(InputInterface $input, OutputInterface $output);
    public function execute();
}