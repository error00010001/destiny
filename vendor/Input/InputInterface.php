<?php

namespace Vendor\Input;

interface InputInterface
{
    public function addArgument($key, $required=false); // по очереди добавлять необходимо
    public function getArgument($key);
    public function getOptions();
}