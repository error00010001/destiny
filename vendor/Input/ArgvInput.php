<?php

namespace Vendor\Input;
use Vendor\Input\InputInterface;
use Vendor\Output\Output;

/**
 * В случае если не устроит данная реализация, можно просто создать другой класс на основании интерфейса и просто
 * в application переопределить класс.
 */
class ArgvInput implements InputInterface
{
    private $tokens;
    private $argumentObjects;
    private $output;

    public function __construct(array $tokens)
    {
        $this->output = new Output();
        array_shift($tokens);
        $this->tokens = $tokens;
        // когда в aplication запускается конструкт этого класс, то у нас заполняется argumentObjects аргументами данное поле
        $this->argumentObjects = [];
        $this->parseArguments();
    }

    /**
     * Потом где это необходимо просто вызываешь этот метод и с помощью array_search() находишь нужную опцию
     * чтобы выполнить необходимый блок кода
     */
    public function getOptions()
    {
        // TODO: Implement getOption() method.
        $options = [];
        foreach ($this->tokens as $token) {
            if (strpos($token, '-') !== false) {
                $options[] = $token;
            }
        }
        return $options;
    }

    public function hasOption($option)
    {
        $options = implode(' ',$this->getOptions());
        return strpos($options,$option) === false ? false : true;
    }

    /**
     * Аргументы не должны содержать - -- --- и тд
     *
     * @param $key
     * @return mixed|null
     */
    public function getArgument($key)
    {
        // TODO: Implement getArgument() method.
        $value = null;
        foreach ($this->argumentObjects as $index => $argumentObject) {
            if ($key === $this->argumentObjects[$index]['key']) {
                $value = $this->argumentObjects[$index]['value'];
                break;
            }
        }
        return $value;
    }

    /**
     * Когда уже сформирован список всех аргументов
     * алгоритм по почереди идет по argumentObjects и вставляет
     * ключи по индексу, чтобы значение по ключу можно было получить
     *
     * !!! Это все по очереди, ломаешь очередность аргументов, ломаешь логику работы команды
     * Можно подумать как лучше сделать добавление аргументов
     *
     * @param $key
     * @param false $required
     * @return $this
     */
    public function addArgument($key, $required=false)
    {
        // добавить еще проверку обязательности
        foreach ($this->argumentObjects as $index => $argumentObject) {
            if (null !== $this->argumentObjects[$index]['value'] && !key_exists('key',$this->argumentObjects[$index])) {
                $this->argumentObjects[$index]['key'] = $key;
                $this->argumentObjects[$index]['required'] = $required;
                break;
            }
        }
        return $this; // для чейнинга
    }

    private function parseArguments()
    {
        foreach ($this->tokens as $token) {
            if (strpos($token, '-') === false) {
                $this->argumentObjects[]['value'] = $token;
            }
        }
    }
}