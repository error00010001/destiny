<?php

namespace Vendor\Output;

/**
 *  случае если хочется писать Output классический
 */
interface OutputInterface
{
    public function write(string $str, $textColor, $bgColor);
    public function fwrite(string $str, $bgColor);
}