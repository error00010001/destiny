<?php

namespace Vendor\Output;

class Output implements OutputInterface
{
    const TEXT_COLORS = [
        'error' => '0;31',
        'warning' => '0;33',
        'info' => '0;37',
        'success' => '0;32'
    ];

    const BACKGROUND_COLORS = [
        'error' => '41',
        'success' => '42',
        'warning' => '43',
        'info' => '47',
    ];

    public function write(string $str, $textColor = 'info', $bgColor = null)
    {
        $string = '';
        if (isset(self::TEXT_COLORS[$textColor])) {
            $string .= "\033[" . self::TEXT_COLORS[$textColor] . "m";
        }
        if (isset(self::BACKGROUND_COLORS[$bgColor])) {
            $string .= "\033[" . self::BACKGROUND_COLORS[$bgColor] . "m";
        }
        $string .= $str . "\033[0m";
        echo $string . PHP_EOL;
    }

    /**
     * Прототип простого форматтера
     *
     * @param string $str
     * @param null $bgColor
     */
    public function fwrite(string $str, $bgColor = null)
    {
        foreach (self::TEXT_COLORS as $TEXT_COLOR => $value) {
            if (strpos($str, "<$TEXT_COLOR>") !== false) {
                preg_match_all('#<'.$TEXT_COLOR.'>(.+?)</'.$TEXT_COLOR.'>#is', $str, $arr);
                $str = str_replace("<$TEXT_COLOR>" . $arr[1][0] . "</$TEXT_COLOR>", ("\033[" . self::TEXT_COLORS[$TEXT_COLOR] . "m" . $arr[1][0] . " \033[0m"), $str);
            }
        }
        echo $str . PHP_EOL;
    }
}